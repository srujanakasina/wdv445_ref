  <div class="site-container">
        
        <div class="gradient-overlay">
        </div>
        
        <!-- header -->
        <div class="header-wrapper container">
            <div class="site-title">
                <p>Photography Blog</p>
            </div>
            
            <div class="menu-wrapper clearfix">
                <?php 
                print theme('links',array('links'=>$main_menu));
            	?>              
            </div>
        </div>
        
        <!-- content -->
        <div class="content-container container">
            
            <div class="featured-photo container">
                <img src="../img/the_birds.jpg" />
                <div class="label">Photo: The birds</div>
            </div>
            
            <!-- tabs will go here -->
            <div class="tab-container container">
            </div>
            
            <div class="title inner-container">
                <h1><?php print $title; ?></h1>
            </div>
            
            <div class="content inner-container clearfix shadow background">
                
                <div class="left-column column region one-fourth left">
                    <div class="region-inner">
                        <?php
                    	print render($page['left_callout']);
                    ?>
                    </div>
                </div>
                
                <div class="main-content three-fourths left">
                    <div class="content">
                        
                        <!-- messages will go here -->
                        <div id="messages">
                        </div>
                        <?php
                    		print render($page['content']);
                		?>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- footer -->
        <div class="footer-container container">
            <div class="footer-content inner-container">
                <?php
                    print render($page['footer']);
                ?>
            </div>
        </div>
        
    </div>

